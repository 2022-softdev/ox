/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.oxx;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OX {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[][] table = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';

            }
        }
        System.out.println("Welcome to OX Game");

        boolean player1 = true;
        boolean gameEnded = false;
        int row = 0;
        int col = 0;
        while (!gameEnded) {
            TableBoard(table);
            if (player1) {
                System.out.println("Turn X");
                System.out.println("Please input row, col: ");
                row = sc.nextInt();
                col = sc.nextInt();
            } else {
                System.out.println("Turn O");
                System.out.println("Please input row, col: ");
                row = sc.nextInt();
                col = sc.nextInt();
            }

            char c = '-';
            if (player1) {
                c = 'X';
            } else {
                c = 'O';
            }
            table[row][col] = c;
            if (playerHasWon(table) == 'X') {
                TableBoard(table);
                System.out.println(">>>X Win<<<");
                gameEnded = true;
            } else if (playerHasWon(table) == 'O') {
                TableBoard(table);
                System.out.println(">>>O Win<<<");
                gameEnded = true;
            } else {
                if (boardIsFull(table)) {
                    TableBoard(table);
                    System.out.println(">>Draw<<");
                    gameEnded = true;
                } else {
                    player1 = !player1;
                }
            }
        }
        
        sc.close();
    }

    public static void TableBoard(char[][] board) {
        System.out.println("");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println();
        }
    }

    public static char playerHasWon(char[][] board) {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1]
                    && board[i][1] == board[i][2]
                    && board[i][0] != '-') {
                return board[i][0];
            }
        }

        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j]
                    && board[1][j] == board[2][j]
                    && board[0][j] != '-') {
                return board[0][j];
            }
        }

        if (board[0][0] == board[1][1]
                && board[1][1] == board[2][2]
                && board[0][0] != '-') {
            return board[0][0];
        }

        if (board[2][0] == board[1][1]
                && board[1][1] == board[0][2]
                && board[2][0] != '-') {
            return board[2][0];
        }

        return ' ';
    }

    public static boolean boardIsFull(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
